fun main (args: Array<String>){
    val a = listOf(3,7,22,8,1)

    a.forEachIndexed { index, i ->
        println("Posisi ke-$index nilainya adalah $i")
    }
    println()

    manuallyForEachIndexed(a, { index, isi ->
        println("Posisi ke-$index nilainya adalah $isi")
    })
    println()

    a.manuallyForEachIndex(){ index, isi ->
        println("Posisi ke-$index nilainya adalah $isi")
    }
}

fun Any.print(){
    print("$this")
}

fun manuallyForEachIndexed (list: List<Int>, isi: (Int, Int) -> Unit){
    for (i in 0 until list.size){
        isi(i, list[i])
    }
}

fun <T> List<T>.manuallyForEachIndex(isi: (Int,T) -> Unit){
    for (i in 0 until this.size){
        isi(i, this[i])
    }
}